[ ] Change host if host disconnects
[ ] Game logic handle disconnected players
[ ] Respect word lists from game settings
[ ] Shorten draw time as players guess correctly
[ ] Better name for SendMessage?
[ ] Add a config file

[x] Block word from chat
[x] Hash client ID to produce playerId
[x] Docker build
[x] Randomize turn order
[x] Implement scoring / points system
[x] Implement word masking in game state
[x] Implement word list
[x] Implement turn order + round sequence -> end game
[x] ModifyMVar instead of take/put
[x] Lobby ticking
[x] Send state timeout to client
[x] Encoding of server messages
[x] perf: Don't mutate state for draw and chat commands
[x] Cleanup unused imports
[x] Parse client messages into actions
[x] Handle client disconnect
[x] Handle lobby clean up after all clients disconnect
[x] Handle client messages (sub MVars?)
[x] Read client ID and name from URL query ("clientId", "name")
