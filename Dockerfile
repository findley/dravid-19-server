FROM haskell:8.8.3 as dependencies
RUN mkdir /opt/build
WORKDIR /opt/build
COPY stack.yaml package.yaml stack.yaml.lock dravid-server.cabal /opt/build/
RUN stack build --system-ghc --dependencies-only

FROM haskell:8.8.3 as build
RUN mkdir /opt/build
WORKDIR /opt/build
COPY --from=dependencies /root/.stack /root/.stack
COPY . /opt/build
RUN stack install --system-ghc --local-bin-path .

FROM alpine:latest
RUN mkdir -p /app
WORKDIR /app
RUN apk update && apk add libpq gmp libc6-compat
COPY --from=build /opt/build/dravid-server-exe ./server
COPY word-lists /app/word-lists
#RUN make build
CMD ["./server"]
