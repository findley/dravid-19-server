module Main where

import Control.Concurrent (MVar, forkIO, modifyMVar, modifyMVar_, newMVar, readMVar, threadDelay)
import Control.Monad (forever, join)
import qualified Data.List as List
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8)
import Data.Time.Clock.POSIX (getPOSIXTime)
import Flow
import Lobby (Lobby, LobbyFactory, LobbyId)
import qualified Lobby
import Model.GameState (PlayerId)
import qualified Network.HTTP.Types.URI as URI
import qualified Util
import qualified Network.WebSockets as WS

data ServerState = ServerState
  { lobbyFactory :: LobbyFactory,
    getLobbys :: Map LobbyId (MVar Lobby)
  }

instance Show ServerState where
  show (ServerState _ lobbys) = "ServerState: " ++ show (Map.keys lobbys)

makeServerState :: IO (ServerState)
makeServerState = do
  factory <- Lobby.makeLobbyFactory
  return <| ServerState factory Map.empty

main :: IO ()
main = do
  s <- makeServerState
  state <- newMVar s
  putStrLn "Starting websocket server on 0.0.0.0:3000"
  forkIO (ticker state)
  WS.runServer "0.0.0.0" 3000 $ application state

ticker :: MVar ServerState -> IO ()
ticker serverState = do
  forever <| do
    threadDelay 500000
    state <- readMVar serverState
    -- timestamp <- (round . (* 1000)) <$> getPOSIXTime
    fmap (Lobby.tick 500) (Map.elems (getLobbys state)) |> mconcat

-- Note that `WS.ServerApp` is nothing but a type synonym for
-- `WS.PendingConnection -> IO ()`.
application :: MVar ServerState -> WS.ServerApp
application state pending = do
  let (path, query) =
        pending |> WS.pendingRequest |> WS.requestPath |> URI.decodePath
  let mLobbyId = parseLobbyId path
  let mClientId = List.lookup "clientId" query |> join
  let mUserName = List.lookup "name" query |> join |> fmap decodeUtf8
  case (mLobbyId, mClientId, mUserName) of
    (Just lobbyId, Just clientId, Just userName) -> do
      conn <- WS.acceptRequest pending
      let playerId = Util.clientIdToPlayerId clientId
      lobby <- getLobby state lobbyId playerId
      Lobby.runClient
        lobby
        conn
        playerId
        userName
        ( \isDone -> do
            if isDone then removeLobby state lobbyId else return ()
        )
    _ -> do
      WS.rejectRequest pending "Bye bye idiot"

getLobby :: MVar ServerState -> LobbyId -> Text -> IO (MVar Lobby)
getLobby state lobbyId playerId = do
  serverState <- readMVar state
  let lobbys = getLobbys serverState
  let existingLobby = Map.lookup lobbyId lobbys
  case existingLobby of
    Just lobby -> do
      return lobby
    Nothing -> do
      newLobby <- addLobby state lobbyId playerId
      return newLobby

addLobby :: MVar ServerState -> LobbyId -> Text -> IO (MVar Lobby)
addLobby state lobbyId playerId = do
  lobby' <- modifyMVar state <| \state -> do
    let factory = lobbyFactory state
    let lobbys = getLobbys state
    let (lobby, factory') = Lobby.newLobby factory lobbyId playerId
    mLobby <- newMVar lobby
    let lobbys' = Map.insert lobbyId mLobby lobbys
    return ((ServerState {lobbyFactory = factory', getLobbys = lobbys'}), mLobby)
  return lobby'

removeLobby :: MVar ServerState -> LobbyId -> IO ()
removeLobby state lobbyId = do
  modifyMVar_ state <| \(ServerState factory lobbys) -> do
    let lobbys' = Map.delete lobbyId lobbys
    return (ServerState factory lobbys')

parseLobbyId :: [Text] -> Maybe LobbyId
parseLobbyId pathParts = case pathParts of
  ["lobby", lobbyId] -> Just lobbyId
  _ -> Nothing
