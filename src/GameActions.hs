module GameActions
  ( startGame,
    chat,
    setSettings,
    draw,
    tick,
    pickWord,
    connectPlayer,
    disconnectPlayer,
  )
where

import Control.Monad.State (State)
import qualified Control.Monad.State as State
import qualified Data.List as List
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import qualified Data.Text as Text
import Flow
import Model.GameSettings (GameSettings)
import qualified Model.GameSettings as Settings
import Model.GameState (GameState, maskGame)
import qualified Model.GameState as G
import Model.ServerMessage
import qualified System.Random as Rand
import qualified WordList
import qualified Util

turnOverMs :: Int
turnOverMs = 8000

gameOverMs :: Int
gameOverMs = 20000

wordSelectMs :: Int
wordSelectMs = 15000

startGame :: G.PlayerId -> GameState -> (GameState, [SendMessage])
startGame playerId game
  | isHost && isPreGame =
    let game' = game |> update |> startWordSelect
     in (game', syncMessages game')
  | otherwise = (game, [])
  where
    isPreGame = G.gameMode game == G.PreGame
    isHost = G.host game == playerId
    update game =
      let playerIds = Map.keys (G.players game)
          (turnOrder, rng') = Util.shuffle (G.rng game) playerIds
       in game
            { G.turnOrder = turnOrder,
              G.scores = Map.map (const 0) (G.players game),
              G.round = 1,
              G.turn = 1,
              G.rng = rng'
            }

pickWord :: Int -> GameState -> (GameState, [SendMessage])
pickWord wordIndex game =
  case G.gameMode game of
    G.WordSelect words ->
      let game' = startDrawing game (words !! wordIndex)
       in (game', syncMessages game')
    _ -> (game, [])

chat :: G.PlayerId -> Text -> GameState -> (GameState, [SendMessage])
chat id msg game
  | isDrawing && canGuess && correctGuess = guessedWord id game
  | isDrawing && correctGuess = (game, []) -- suppress word in chat messages
  | otherwise = (game, [SendMessage (ToAllExcept id) (userChat id msg)])
  where
    isDrawing = G.gameMode game == G.Drawing
    canGuess = id /= G.drawer game && not (Map.member id (G.winners game))
    correctGuess = normalizeWord (G.word game) == normalizeWord msg
    normalizeWord = Text.strip . Text.toLower

guessedWord :: G.PlayerId -> GameState -> (GameState, [SendMessage])
guessedWord id game =
  let (game', msgs)
        | allGuessed = updateGame game |> next
        | otherwise = (updateGame game, [])
   in (game', sysMsg : syncMessages game' ++ msgs)
  where
    name = Map.lookup id (G.players game) |> fmap G.name |> fromMaybe "<unknown>"
    sysMsg = SendMessage ToAll (systemChat (name <> " guessed the word!") "green")
    allGuessed = Map.size (G.winners game) + 1 == List.length (G.turnOrder game) - 1
    timeLeftMs = G.timeRemainingMs game
    drawTimeMs = G.settings game |> Settings.drawTime |> (* 1000)
    guesserPoints = calcGuesserPoints drawTimeMs timeLeftMs
    drawerPoints = calcDrawerPoints (List.length (G.turnOrder game))
    updateGuesserScore scores = Map.adjust (+ guesserPoints) id scores
    updateDrawerScore scores = Map.adjust (+ drawerPoints) (G.drawer game) scores
    updateGame game =
      game
        { G.winners = Map.insert id guesserPoints (G.winners game),
          G.scores = (G.scores game) |> updateDrawerScore |> updateGuesserScore
        }

calcGuesserPoints :: Int -> Int -> Int
calcGuesserPoints drawTime timeRemaining =
  (fromIntegral (maxPoints - minPoints)) * r + fromIntegral minPoints |> round
  where
    maxPoints = 100
    minPoints = 20
    r = (fromIntegral timeRemaining) / (fromIntegral drawTime)

calcDrawerPoints :: Int -> Int
calcDrawerPoints playerCount =
  maxPoints / fromIntegral (playerCount - 1) |> round
  where
    maxPoints = 200

setSettings :: G.PlayerId -> GameSettings -> GameState -> (GameState, [SendMessage])
setSettings playerId settings game
  | isHost && isPreGame =
    case G.gameMode game of
      G.PreGame ->
        let game' = update game
         in (game', syncMessages game')
      _ -> (game, [])
  | otherwise = (game, [])
  where
    isHost = G.host game == playerId
    isPreGame = G.gameMode game == G.PreGame
    update game = game {G.settings = settings}

draw :: G.PlayerId -> Text -> GameState -> (GameState, [SendMessage])
draw playerId drawData game
  | isDrawing && isDrawer = (game, [msg])
  | otherwise = (game, [])
  where
    isDrawer = G.drawer game == playerId
    isDrawing = G.gameMode game == G.Drawing
    msg = SendMessage (ToAllExcept (G.drawer game)) (Draw drawData)

tick :: Int -> GameState -> (GameState, [SendMessage])
tick deltaMs game
  | isPreGame = (game, [])
  | timeRemaining <= 0 =
    let (game', msgs) = next game
     in (game', msgs ++ syncMessages game')
  | otherwise =
    let game' = updateGameSeconds game
     in (game', [])
  where
    isPreGame = G.gameMode game == G.PreGame
    timeRemaining = G.timeRemainingMs game - deltaMs
    updateGameSeconds game = game {G.timeRemainingMs = G.timeRemainingMs game - deltaMs}

disconnectPlayer :: G.PlayerId -> GameState -> (GameState, [SendMessage])
disconnectPlayer playerId game =
  let game' = game {G.players = updatePlayers (G.players game)}
      name = Map.lookup playerId (G.players game) |> fmap G.name |> fromMaybe "<unknown>"
      sysMsg = SendMessage (ToAllExcept playerId) (systemChat (name <> " has disconnected") "#555555")
   in (game', sysMsg : syncMessages game')
  where
    updatePlayers players =
      case G.gameMode game of
        G.PreGame ->
          Map.delete playerId players
        _ ->
          Map.adjustWithKey updatePlayer playerId players
    updatePlayer _ p = p {G.connected = False}

connectPlayer :: G.PlayerId -> Text -> GameState -> (GameState, [SendMessage])
connectPlayer playerId name game =
  case G.gameMode game of
    G.PreGame ->
      let (color, rng') = Rand.randomR (0, 100) (G.rng game)
          player = G.Player name playerId True color
          game' = game {G.players = Map.insert playerId player (G.players game), G.rng = rng'}
          sysMsg = SendMessage (ToAllExcept playerId) (systemChat (name <> " has joined the game") "#555555")
       in (game', sysMsg : syncMessages game')
    _ ->
      if Map.member playerId (G.players game)
        then
          let game' = game {G.players = Map.adjustWithKey (\_ p -> p {G.connected = True}) playerId (G.players game)}
              sysMsg = SendMessage (ToAllExcept playerId) (systemChat (name <> " has reconnected") "#555555")
           in (game', sysMsg : syncMessages game')
        else (game, [SendMessage (To playerId) (Bye "Game is already in progress")])

next :: G.GameState -> (G.GameState, [SendMessage])
next game = case G.gameMode game of
  G.WordSelect words ->
    let (i, rng') = Rand.randomR (0, List.length words - 1) (G.rng game)
     in (startDrawing (game {G.rng = rng'}) (words !! i), [])
  G.Drawing ->
    let msg = systemChat ("The word was: " <> (G.word game)) "black"
     in (startTurnOver game, [SendMessage ToAll msg])
  G.TurnOver -> (game |> advanceTurn |> startWordSelect, [])
  G.GameOver -> (game {G.gameMode = G.PreGame, G.winners = Map.empty}, [])
  _ -> (game, [])

advanceTurn :: G.GameState -> G.GameState
advanceTurn game =
  game {G.turn = turn', G.round = round'}
  where
    (round', turn') = nextRoundAndTurn game

nextRoundAndTurn :: G.GameState -> (Int, Int)
nextRoundAndTurn game =
  (round', turn')
  where
    turn' = ((G.turn game) `mod` List.length (G.turnOrder game)) + 1
    round'
      | turn' == 1 = G.round game + 1
      | otherwise = G.round game

startWordSelect :: G.GameState -> G.GameState
startWordSelect game =
  game
    { G.gameMode = G.WordSelect randomWords,
      G.timeRemainingMs = wordSelectMs,
      G.winners = Map.empty,
      G.drawer = (G.turnOrder game) !! ((G.turn game) - 1),
      G.word = "",
      G.rng = g''
    }
  where
    (g', g'') = Rand.split (G.rng game)
    randomWords = WordList.randomWords (G.wordLists game) (g') 3

startDrawing :: G.GameState -> Text -> G.GameState
startDrawing game word =
  game
    { G.gameMode = G.Drawing,
      G.word = word,
      G.timeRemainingMs = game |> G.settings |> Settings.drawTime |> (* 1000)
    }

startTurnOver :: G.GameState -> G.GameState
startTurnOver game
  | isOver = game {G.gameMode = G.GameOver, G.timeRemainingMs = gameOverMs}
  | otherwise = game {G.gameMode = G.TurnOver, G.timeRemainingMs = turnOverMs}
  where
    (round', _) = nextRoundAndTurn game
    isOver = round' > Settings.rounds (G.settings game)

syncMessages :: G.GameState -> [SendMessage]
syncMessages game = case G.gameMode game of
  G.WordSelect _ -> messages
  G.Drawing -> messages
  _ -> [SendMessage ToAll (Sync game)]
  where
    drawer = G.drawer game
    drawerMessage = SendMessage (To drawer) (Sync game)
    guesserMessage = SendMessage (ToAllExcept drawer) (Sync <| maskGame game)
    messages = [drawerMessage, guesserMessage]
