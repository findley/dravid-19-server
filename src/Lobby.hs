module Lobby
  ( Lobby,
    LobbyId,
    LobbyFactory,
    newLobby,
    makeLobbyFactory,
    runClient,
    tick,
  )
where

import Control.Concurrent (MVar, modifyMVar, putMVar, takeMVar)
import Control.Exception (finally)
import Control.Monad (forever)
import Control.Monad.State (runState)
import qualified Data.Aeson as Aeson
import Data.Foldable (forM_)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (Text)
import Debug.Trace (traceM)
import Flow
import qualified GameActions as Actions
import Model.ClientMessage (ClientMessage)
import qualified Model.ClientMessage as ClientMessage
import Model.GameState (GameState, PlayerId, newGame)
import Model.ServerMessage
import qualified Network.WebSockets as WS
import qualified Util
import System.Random as Rand
import WordList (WordLists, loadWordLists)

type LobbyId = Text

data Lobby = Lobby
  { _id :: LobbyId,
    clients :: Map PlayerId WS.Connection,
    gameState :: GameState
  }

instance Show Lobby where
  show (Lobby id clients state) =
    "Lobby {id = " ++ show id
      ++ ", clients = ["
      ++ mconcat (fmap show (Map.keys clients))
      ++ "], state = "
      ++ show state
      ++ "}"

data LobbyFactory = LobbyFactory
  { rng :: Rand.StdGen,
    wordLists :: WordLists
  }

makeLobbyFactory :: IO (LobbyFactory)
makeLobbyFactory = do
  rng <- Rand.newStdGen
  lists <- loadWordLists
  return <| LobbyFactory {rng = rng, wordLists = lists}

newLobby :: LobbyFactory -> LobbyId -> PlayerId -> (Lobby, LobbyFactory)
newLobby factory lobbyId playerId =
  (Lobby {_id = lobbyId, clients = Map.empty, gameState = game}, factory')
  where
    (g1, g2) = Rand.split (rng factory)
    factory' = factory {rng = g1}
    game = newGame playerId g2 (wordLists factory)

runClient :: MVar Lobby -> WS.Connection -> PlayerId -> Text -> (Bool -> IO ()) -> IO ()
runClient mLobby conn playerId name cleanup = do
  (lobby, msgs) <- modifyLobby addClient connectAction mLobby
  fmap (broadcast lobby) msgs |> mconcat
  WS.withPingThread conn 5 (return ())
    <| clientLoop conn playerId mLobby cleanup
  where
    connectAction = Actions.connectPlayer playerId name
    addClient lobby =
      lobby {clients = Map.insert playerId conn (clients lobby)}

clientLoop :: WS.Connection -> PlayerId -> MVar Lobby -> (Bool -> IO ()) -> IO ()
clientLoop conn playerId mLobby cleanup =
  flip finally (disconnect mLobby playerId cleanup) <| do
    forever <| do
      msg <- WS.receiveData conn
      case fmap (msgToAction playerId) (Aeson.decode msg) of
        Just action -> do
          (lobby, msgs) <- modifyLobby id action mLobby
          fmap (broadcast lobby) msgs |> mconcat
        Nothing -> return ()

disconnect :: MVar Lobby -> PlayerId -> (Bool -> IO ()) -> IO ()
disconnect mLobby playerId cleanup = do
  (lobby, msgs) <- modifyLobby removeClient disconnectAction mLobby
  fmap (broadcast lobby) msgs |> mconcat
  cleanup (Map.null (clients lobby))
  where
    disconnectAction = Actions.disconnectPlayer playerId
    removeClient lobby =
      lobby {clients = Map.delete playerId (clients lobby)}

tick :: Int -> MVar Lobby -> IO ()
tick deltaMs mLobby = do
  (lobby, msgs) <- modifyLobby id (Actions.tick deltaMs) mLobby
  fmap (broadcast lobby) msgs |> mconcat

modifyLobby :: (Lobby -> Lobby) -> (GameState -> (GameState, [SendMessage])) -> MVar Lobby -> IO (Lobby, [SendMessage])
modifyLobby lobbyUpdater action mLobby =
  modifyMVar mLobby <| \lobby -> do
    let (state', msgs) = action (gameState lobby)
    let lobby' = lobby {gameState = state'} |> lobbyUpdater
    return (lobby', (lobby', msgs))

msgToAction :: PlayerId -> ClientMessage -> (GameState -> (GameState, [SendMessage]))
msgToAction playerId msg =
  case msg of
    ClientMessage.Chat contents -> Actions.chat playerId contents
    ClientMessage.Draw drawString -> Actions.draw playerId drawString
    ClientMessage.PickWord wordIndex -> Actions.pickWord wordIndex
    ClientMessage.StartGame -> Actions.startGame playerId
    ClientMessage.Settings settings -> Actions.setSettings playerId settings

broadcast :: Lobby -> SendMessage -> IO ()
broadcast lobby message =
  case message of
    SendMessage (To playerId) msg -> do
      let maybeConn = Map.lookup playerId (clients lobby)
      forM_ maybeConn (sendMessage msg)
    SendMessage (ToAllExcept playerId) msg -> do
      let filter pid _ = pid /= playerId
      let filteredConns = Map.filterWithKey filter (clients lobby)
      let conns = Map.elems filteredConns
      fmap (sendMessage msg) conns |> mconcat
    SendMessage ToAll msg ->
      fmap (sendMessage msg) (Map.elems (clients lobby)) |> mconcat

sendMessage :: Message -> WS.Connection -> IO ()
sendMessage message conn = WS.sendTextData conn <| Aeson.encode message
