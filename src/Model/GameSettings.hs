{-# LANGUAGE DeriveGeneric #-}

module Model.GameSettings
  ( GameSettings (..),
    defaultSettings,
  )
where

import qualified Data.Aeson as Aeson
import Data.Text
import GHC.Generics
import Model.Encoding
import WordList (defaultWordList)

data GameSettings = GameSettings
  { rounds :: Int,
    drawTime :: Int,
    lists :: [Text]
  }
  deriving (Show, Generic)

defaultSettings :: GameSettings
defaultSettings =
  GameSettings {rounds = 3, drawTime = 80, lists = [defaultWordList]}

instance Aeson.FromJSON GameSettings where
  parseJSON =
    Aeson.genericParseJSON encodingOptions

instance Aeson.ToJSON GameSettings where
  toEncoding =
    Aeson.genericToEncoding encodingOptions
