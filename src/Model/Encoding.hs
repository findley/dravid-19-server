module Model.Encoding (encodingOptions) where

import qualified Data.Aeson as Aeson

encodingOptions :: Aeson.Options
encodingOptions =
  Aeson.defaultOptions
    { Aeson.fieldLabelModifier = Aeson.camelTo2 '_',
      Aeson.constructorTagModifier = Aeson.camelTo2 '_',
      Aeson.sumEncoding =
        Aeson.TaggedObject
          { Aeson.tagFieldName = "type",
            Aeson.contentsFieldName = "content"
          }
    }
