{-# LANGUAGE DeriveGeneric #-}

module Model.GameState
  ( GameState (..),
    GameMode (..),
    newGame,
    maskGame,
    Player (..),
    PlayerId,
  )
where

import qualified Data.Aeson as Aeson
import Data.Aeson ((.=))
import qualified Data.Char as Char
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (Text)
import qualified Data.Text as Text
import Flow
import GHC.Generics
import Model.Encoding
import qualified Model.GameSettings as Settings
import System.Random
import Prelude hiding (round)
import WordList (WordLists)

type PlayerId = Text

data GameState = GameState
  { gameMode :: GameMode,
    settings :: Settings.GameSettings,
    round :: Int,
    turn :: Int,
    drawer :: PlayerId,
    host :: PlayerId,
    word :: Text,
    timeRemainingMs :: Int,
    winners :: Map PlayerId Int,
    scores :: Map PlayerId Int,
    players :: Map PlayerId Player,
    turnOrder :: [PlayerId],
    rng :: StdGen,
    wordLists :: WordLists
  }
  deriving (Show)

data GameMode
  = PreGame
  | WordSelect [Text]
  | Drawing
  | TurnOver
  | GameOver
  deriving (Eq, Show, Generic)

data Player = Player
  { name :: Text,
    id :: PlayerId,
    connected :: Bool,
    color :: Int
  }
  deriving (Show, Generic)

newGame :: PlayerId -> StdGen -> WordLists -> GameState
newGame playerId rng lists =
  GameState
    { gameMode = PreGame,
      settings = Settings.defaultSettings,
      round = 1,
      turn = 1,
      host = playerId,
      drawer = "",
      word = "",
      timeRemainingMs = 0,
      winners = Map.fromList [],
      scores = Map.fromList [],
      turnOrder = [],
      players = Map.fromList [],
      rng = rng,
      wordLists = lists
    }

maskGame :: GameState -> GameState
maskGame game =
  case gameMode game of
    WordSelect _ -> game |> maskWordOptions |> maskWordField
    Drawing -> maskWordField game
    _ -> game
  where
    maskWordOptions game = game {gameMode = WordSelect []}
    maskWordField game = game {word = maskWord (word game)}

maskWord :: Text -> Text
maskWord word =
  Text.map convertChar word
  where
    convertChar ch =
      case Char.isAlphaNum ch of
        True -> '_'
        False -> ch

instance Aeson.ToJSON GameMode where
  toEncoding = Aeson.genericToEncoding encodingOptions

instance Aeson.ToJSON GameState where
  toJSON game = Aeson.object [] -- wtf?
  toEncoding game =
    Aeson.pairs
      ( "game_mode" .= gameMode game
          <> "settings" .= settings game
          <> "round" .= round game
          <> "turn" .= turn game
          <> "drawer" .= drawer game
          <> "host" .= host game
          <> "word" .= word game
          <> "time_remaining_ms" .= timeRemainingMs game
          <> "winners" .= winners game
          <> "scores" .= scores game
          <> "players" .= players game
          <> "turn_order" .= turnOrder game
      )

instance Aeson.ToJSON Player where
  toEncoding = Aeson.genericToEncoding encodingOptions
