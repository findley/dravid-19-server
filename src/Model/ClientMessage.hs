{-# LANGUAGE DeriveGeneric #-}

module Model.ClientMessage
  ( ClientMessage (..),
  )
where

import qualified Data.Aeson as Aeson
import Data.Text (Text)
import GHC.Generics
import Model.Encoding
import Model.GameSettings

data ClientMessage
  = Draw Text
  | PickWord Int
  | Chat Text
  | StartGame
  | Settings GameSettings
  deriving (Generic, Show)

instance Aeson.FromJSON ClientMessage where
  parseJSON =
    Aeson.genericParseJSON encodingOptions
