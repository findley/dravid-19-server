{-# LANGUAGE DeriveGeneric #-}

module Model.ServerMessage
  ( SendMessage (..),
    Recipient (..),
    Message (..),
    ChatMsg (..),
    systemChat,
    userChat,
  )
where

import qualified Data.Aeson as Aeson
import Data.Text (Text)
import GHC.Generics
import Model.Encoding
import qualified Model.GameState as G

data SendMessage = SendMessage
  { to :: Recipient,
    msg :: Message
  }
  deriving (Show)

data Recipient
  = To G.PlayerId
  | ToAllExcept G.PlayerId
  | ToAll
  deriving (Show)

data Message
  = Sync G.GameState
  | Chat ChatMsg
  | Draw Text
  | Bye Text
  deriving (Generic, Show)

data ChatMsg = ChatMsg
  { playerId :: G.PlayerId,
    message :: Text,
    system :: Bool,
    color :: Maybe Text
  }
  deriving (Generic, Show)

systemChat :: Text -> Text -> Message
systemChat msg color =
  Chat ChatMsg {playerId = "", message = msg, system = True, color = Just color}

userChat :: G.PlayerId -> Text -> Message
userChat playerId msg =
  Chat ChatMsg {playerId = playerId, message = msg, system = False, color = Nothing}

instance Aeson.ToJSON Message where
  toEncoding = Aeson.genericToEncoding encodingOptions

instance Aeson.ToJSON ChatMsg where
  toEncoding = Aeson.genericToEncoding encodingOptions
