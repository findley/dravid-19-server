module Util (shuffle, clientIdToPlayerId) where

import Crypto.Hash.SHA256 (hash)
import Data.ByteString (ByteString)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8)
import Data.ByteString.Base16 (encode)
import qualified Data.Text as Text
import System.Random as Rand

shuffle :: Rand.RandomGen g => g -> [a] -> ([a], g)
shuffle gen [] = ([], gen)
shuffle gen l =
  toElems $ foldl fisherYatesStep (initial (head l) gen) (numerate (tail l))
  where
    toElems (x, y) = (Map.elems x, y)
    numerate = zip [1 ..]
    initial x gen = (Map.singleton 0 x, gen)

fisherYatesStep :: Rand.RandomGen g => (Map Int a, g) -> (Int, a) -> (Map Int a, g)
fisherYatesStep (m, gen) (i, x) = ((Map.insert j x . Map.insert i (m Map.! j)) m, gen')
  where
    (j, gen') = Rand.randomR (0, i) gen

clientIdToPlayerId :: ByteString -> Text
clientIdToPlayerId =
  Text.take 10 . decodeUtf8 . encode . hash
