module WordList (WordLists, loadWordLists, defaultWordList, randomWords) where

import qualified Data.List as List
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import Flow
import qualified System.Random as Rand
import System.Directory (listDirectory)

newtype WordLists = WordLists {getWordLists :: Map Text [Text]}

instance Show WordLists where
  show (WordLists lists) =
    "[" ++ mconcat (fmap show (Map.keys lists)) ++ "]"

defaultWordList :: Text
defaultWordList = "general"

wordListDirectory :: String
wordListDirectory = "./word-lists"

loadWordLists :: IO (WordLists)
loadWordLists = do
  wordFiles <- listDirectory wordListDirectory
  lists <- mapM fn wordFiles
  return <| WordLists <| Map.fromList lists
  where
    fn :: String -> IO (Text, [Text])
    fn name = do
      contents <- Text.readFile (wordListDirectory ++ "/" ++ name)
      return (Text.pack name, Text.lines contents)

randomWords :: Rand.RandomGen g => WordLists -> g -> Int -> [Text]
randomWords wordLists rng num =
  List.unfoldr (Just . (randomWord wordLists)) rng |> take num

randomWord :: Rand.RandomGen g => WordLists -> g -> (Text, g)
randomWord wordLists rng =
  (list !! wordIndex, rng'')
  where
    lists = getWordLists wordLists
    (listIndex, rng') = Rand.randomR (0, Map.size lists - 1) rng
    (_, list) = Map.elemAt listIndex lists
    (wordIndex, rng'') = Rand.randomR (0, List.length list - 1) rng'
